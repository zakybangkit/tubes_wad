<?php



// semua daftar route dapat dilihat pada php artisan route:list

Route::get('/', function () {
    return view('welcome');
});
// ini merupakan url untuk login dan register

Auth::routes();
 /*
    ini link route pasca login untuk mengaksesnya gunakan prefix home terlebih dahulu
    misal untuk routing dengan parameter /todo maka untuk mengaksesnya mengunakan localhost:8000/home/todo
 */
Route::prefix('home')->group(function(){
    
 /*
        

        url slash atau / digunakan menampilkan halaman default pasca login dimana halaman yang ditampilkan 
        adalah halaman dengan beragam todo yang sudah dibuat
*/

Route::get('/','HomeController@index')->name('home');
/*
    

        url todo digunakan untuk menampilkan halaman create letak fungsi parameter todo
        terletak pada TodolistController dengan method bernama index dimana, methodnya akan 
        mengemablikan halaman create pada folder resources -> views -> todolists -> create.blade.php
        yang kemudian akan di tampilkan pada halaman web
    */
    
    Route::get('/todo', 'TodolistController@index')->name('todo');
    /*
    

        url /post/create fungsinya untuk melakukan "store" nilai dari hasil input dari
        halaman create ke database todolist dengan tempat tabel yang di tuju adalah tabel todolists
        logikannya terletak pada HomeController dengan method show
    */
    Route::post('/post/create', 'TodolistController@create')->name('create');

    /*
        tugas : valdis

        url about digunakan untuk menampilkan halaman tentang letak fungsi parameter about
        terletak pada HomeController dengan method bernama about dimana, methodnya akan 
        mengemablikan halaman about pada folder resources -> views -> about.blade.php
        yang kemudian akan di tampilkan pada halaman web
    */
    Route::get('/about', 'HomeController@about')->name('about');

/*
    url /edit/{id} digunakan untuk menampilkan halaman form pada halaman pada todolists -> update.blade.php 
        dengan nilai yang akan di edit tanda {} pada {id} mewakili parameter dari nomor todolist yang ingin diupdate
        logikannya terletak pada HomeController dengan method about
     */
    Route::get('/edit/{id}', 'HomeController@show')->name('edit');
    /*
    
        url ini post/update ini berfungsi untuk melakukan update kepada tiap post yang ada dalam database 
        tombol ini dipicu pasca memencet tombol update halaman update.blade.php
        logikannya terletak pada TodolistController dengan method destroy
    */
    Route::put('/post/update/{id}','TodolistController@update')->name('update_post');

    /*
        
        url /post/create ini berfungsi untuk melakukan penghapusan todolist dengan parameter {id} yang terkait pada 
        tiap post {id} sifatnya unique artinya nilai id post saling berbeda satu sama lain
        tidak ada halaman khusus untuk mengakses url ini, hanya di butuhkan button atau tombol untuk 
        memicu setiap kejadian delete pada setiap todolist dan tombol itu terletak pada halaman home pasca 
        login yaitu tombol pada setiap todolist bernama delete
        logikannya terletak pada TodolistController dengan method create
    */
    Route::delete('/post/delete/{id}', 'TodolistController@destroy')->name('delete_post');

    /*
        
        url users/update berfungsi untuk melakukan menampilkan halaman form update users.
        logikannya terletak pada HomeController dengan method update_user 
    */
    Route::get('/user/update/{id}', 'HomeController@edit_user')->name('edit_user');
    /*
        
        url users/store berfungsi untuk melakukan perubahan data user pada database dengan kolom users
         logikannya terletak pada HomeController dengan method store_user
    */
    Route::put('/user/update', 'HomeController@update_user')->name('update_user');
    
});

