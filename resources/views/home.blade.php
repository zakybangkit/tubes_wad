<!-- ini merupakan halaman pasca login -->
@extends('layouts.app')
@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-9">
        <a href="{{ Route('todo')}}">
                        <button class="btn mb-5" style="background : #64DCE5">Input Schedule</button>
                </a>
        </div>
    </div>
    <!-- looping todolists sebagai todolist -->
    @foreach ($todolists as $todolist)
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card mb-5">
                <!-- print nilai todolist bernama slug -->
                <div class="card-header" style="background : #64DCE5">{{$todolist->slug}}</div>
                <div class="card-body">
                <!-- print nilai todolist bernama description -->
                <p>{{$todolist->description}}</p>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card">
            <!-- print nilai todolist bernama date -->
            <div class="card-header text-center">{{$todolist->date}}</div>
                <div class="card-body" style="background : black">
                   <div class="row" >
                       <div class="col-md-4 offset-md-2">
                       <!-- menghapus data -->
                       <form action="{{ Route('delete_post', $todolist->id) }}" method="post">
                            @csrf
                            @method('delete')
                            <button class="btn btn-danger">Delete</button>
                        </form>
                        
                       </div>
                       <div class="col-md-4">
                       <!-- pergi ke halaman edit -->
                       <a href="{{ Route('edit', $todolist->id) }}">
                            <button class="btn btn-success">Update</button>
                        </a>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach 
</div>
@endsection
