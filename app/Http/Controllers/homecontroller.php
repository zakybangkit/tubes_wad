<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todolist;
use App\User;
use Auth;
class HomeController extends Controller
{
   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        /* 
           query mendapatkan data Todolist dari user terkait mengunaka methode DESCENDING 
           dengan mengunakan eloquent ORM dimana setiap nilai yang di dapatkan berdasarkan id 
           user pasca login
        */
        $todolists = Todolist::where('user_id', Auth::id())->orderBy('id', 'DESC')->get();
        // mengembalikan nilai pada halaman 'views/home dengan nilai lemparan bernama todolists atau hasil query mengunakan metode compact
        return view('home', compact('todolists'));
    }
    
    public function about(){
        /*
            
            fungsi ini hanya menampilkan halaman about
        */
        return view('about');
    }
    
    public function show(Todolist $todolist, $id){
    
    /*
        
            fungsi show berfungsi untuk menampilkan tampilan halaman form update todolist
        */
        // memilih todolist berdasarkan berdasarkan id terkait
        $todo = $todolist->where('id', $id)->get();
        // melempar nilai query dengan variabel todo mengunakan method bernama compact pada halaman todolist.update
        return view('todolist.update', compact('todo'));
    }

    public function edit_user($id){

        /*
           
            fungsi edit_user berfungsi untuk menampilkan form edit user 
        */
        // memilih User mengunakan model Eloquent user dimana id sama dengan parameter user id
        $user = User::where('id', $id)->get();
        // melempar nilai query dengan variabel user mengunakan method bernama compact pada halaman users.edit
        return view('users.edit', compact('user'));
    }

    public function update_user(Request $request){
        /*
            
            fungsi update_user berfungsi untuk melakukan perubahan data user pada database di kolom users
        */
        // mendapatkan nilai halaman users.edit dengan input bernama phone_number
        $phone = $request->input('phone_number');
        // mendapatkan nilai halaman users.edit dengan input bernama address
        $address = $request->input('address');
        // mendapatkan nilai halaman users.edit dengan input bernama birthdate
        $birthdate = $request->input('birthdate');
        // memilih User menggunakan model Eloquent user dimana id sama dengan nilai id pasca login 
        // jika di dapatkan lakukan update pada 3 nilai yaitu phone_number, address dan birthdate
        $exec = User::where('id', Auth::id())->update([
            'phone_number' => $phone,
            'address' => $address,
            'birthdate' => $birthdate
        ]);
        // jika query berhasil di eksekusi
        if($exec){
            // kembali pada routing home
                return redirect('home');
        }else{
            // tampilkan string error
            return 'something error!';
        }
    }
}