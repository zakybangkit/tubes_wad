<?php

namespace App\Http\Controllers;

use App\Todolist;
use Illuminate\Http\Request;
use Auth;
class TodolistController extends Controller
{
    public function __construct(Todolist $todo)
    {  
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
            fungsi ini berfungsi untuk menampilkan halaman dari view todolist.create
        */
        return view('todolist.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        /*
            fungsi ini berfungsi untuk melakukan store data todolist pada database dengan kolom todolists
        */
        // deklarasi model
        $todo = new Todolist;
        // mengisi nilai persiapan dengan parameter id pasca login
        $todo->user_id = Auth::id();
        // mengisi nilai persiapan dengan parameter slug
        $todo->slug  = $request->input('slug');
        // mengisi nilai persiapan dengan parameter description
        $todo->description = $request->input('description');
        // mengisi nilai persiapan dengan parameter date
        $todo->date = $request->input('date');
        // melempar nilai pada halaman database
        $exec = $todo->save();
        // jika query berhasil
        if($exec){
            // kembali ke routing dengan alias home
            return redirect('home');
        }else{
            // kembali ke routing dengan alias create
            return redirect('create');
        }
    }

    public function update(Request $request, Todolist $todolist, $id)

    {
         /*
            
            fungsi ini berfungsi untuk melakukan update pada database terhadap nilai todolist yang ada
        */
        //memilih todolist dengan id sama dengan id todolist yang tersedia
        // jika iya ganti nilai berdasarkan hasil input dari halaman update.blade.php 
        $exec = Todolist::where('id', $id)->update([
            'slug' => $request->input('slug'),
            'description' => $request->input('description'),
            'date' => $request->input('date')
        ]);
        // jika query berhasil kembali ke halaman home jika tidak kembalikan string error
        if($exec){
            return redirect('home');
        }else{
            return 'something error';
        }
    }

   
    public function destroy($id)
    {
        /*
            
            fungsi ini berfungsi untuk melakukan delete pada database terhadap nilai todolist yang ada
        */
        // memilih id berdasarakan id post jika iya maka hapus
        $exec = Todolist::where('id', $id)->delete();
        // jika query berhasil maka kembali ke halaman semua jika tidak print string error
        if($exec){
            return redirect('home');
        }else{
            return 'something wrong';
        }
    }
}