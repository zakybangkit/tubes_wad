<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodolistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // membuat tabel todolists
        Schema::create('todolists', function (Blueprint $table) {
            // menciptakan kolom dengan nama id dan bersifat autoincrement
            $table->bigIncrements('id');
            // menciptakan kolom dengan nama user_id
            $table->unsignedBigInteger('user_id');
            // menciptakan kolom dengan nama slug
            $table->string('slug');
            // menciptakan kolom dengan nama description
            $table->string('description');
            // menciptakan kolom dengan nama date
            $table->string('date');
            // menciptakan kolom dengan nama create_at dan updated_at
            $table->timestamps();
            // menghubungkan foreign_key user_id dengan referensi dari kolom user dengan kolom id 
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todolists');
    }
}