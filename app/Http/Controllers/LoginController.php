<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(){
        return view('users.login');
    }
     public function register(){
        return view('users.register');
    }

    public function create(){
        return View::make('auth.login');
    }
    public function user(){
        $list = array(
            'email' => 'required',
            'password' => 'required',
        );

    $Validasi = Validator::make(Input::all(), $list);

    if ($validator->fails()) {
        return Redirect::to('users/login')
         ->withErrors($validator)
         ->withInput(Input::except('password'));
       } else {
           //login
        $login             = new Login;
        $login->email      = Input::get('email');
        $login->password    = Input::get('password');
        $login->save();
     
        // redirect
        Session::flash('Input data sukses!');
        return Redirect::to(';login');
       }
      }
    }

