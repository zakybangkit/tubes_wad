@extends('global')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="container">
                <div class="card" style="background-color: #EFDED8;">
                    <h2 class="text-center">Sign In</h2>
                    <form class="form-horizontal" action='' method="POST">
                        <fieldset class="container">
                         
                          <div class="control-group">
                            <!-- Username -->
                            <label class="control-label"  for="username">Username</label>
                            <div class="controls">
                              <input type="text" id="username" name="username" placeholder="" class="form-control">
                            </div>
                          </div>
                       
                          <div class="control-group">
                            <!-- E-mail -->
                            <label class="control-label" for="email">E-mail</label>
                            <div class="controls">
                              <input type="text" id="email" name="email" placeholder="" class="form-control">
                            </div>
                          </div>
                       
                          <div class="control-group">
                            <!-- Password-->
                            <label class="control-label" for="password">Password</label>
                            <div class="controls">
                              <input type="password" id="password" name="password" placeholder="" class="form-control">
                            </div>
                          </div>      
                        </fieldset>

                        <div class="text-center mb-5 container">
                            <button class="btn btn-success btn-lg btn-block mt-5 " style="background-color : #EC9696; color : #707070 ">Create</button>
                        </div>
                
                    </div>
            </div>
        </div>
    </div>
@endsection