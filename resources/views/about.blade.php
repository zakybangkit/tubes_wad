<!-- ini merupakan halaman about -->
@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
            <div class="card-header">
                <h1>About</h1>
            </div>
                <div class="card-body">
                    <h3>
                        This the todo list app, you can write all you todo list task here
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection