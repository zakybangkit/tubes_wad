@extends('layouts.app')
@section('content')
    <div class="container">
    <!-- form halaman create todo -->
    <form method="post" action="{{ Route('create')}}">
                @csrf
                {{-- @method('post') --}}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                    <label for="slug">Input Todo Name</label>
                                    <input type="text" placeholder="Input Todo Name" name="slug" class="form-control" style="background : #64DCE5">
                            </div>
                        </div>
                        <label for="description" class="mt-3" >Description</label><br>
                        <textarea name="description" id="textarea" cols="100" rows="10" style="background: #EFDED8"></textarea>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Date: <input type="date" id="datepicker" class="form-control" name="date"></p>
                            </div>
                            <div class="col-md-6">
                            <!-- tombol pemicu create todo -->
                                    <button type="submit" class="btn btn-lg mt-4" style="margin-left : 180px; background-color : #ec9696">Create</button>
                            </div>
                        </div>
                    </div>
                    
        </form>
    </div>
@endsection