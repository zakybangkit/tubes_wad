<!-- ini adalah halaman untuk melakukan edit terhadap user -->
@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <!-- form update halaman user -->
           <form action="{{ route('update_user') }}" method="post">
            @csrf
            @method('put')
               @foreach($user as $idx)
                <div>
                <label for="phone_number">Phone number</label>
               <input type="text" class="form-control" name="phone_number" value="{{ $idx->phone_number}}" placeholder="Please input your phone number">
                </div>
                <div>
                <label for="phone_number">Address</label>
                <textarea type="text" class="form-control" name="address" value="{{ $idx->address}}" placeholder="Please input your address">{{$idx->address}}</textarea>
                </div>

                <div>
                <label for="phone_number">Birthdate</label>
               <input type="date" class="form-control" name="birthdate" value="{{ $idx->birthdate}}" placeholder="Please input your birth date">
                </div>
               @endforeach
               <!-- tombol pemicu update user -->
               <button type="submit" class="btn btn-primary btn-lg mt-5">Update User</button>
           </form>
        </div>
    </div>
</div>
@endsection