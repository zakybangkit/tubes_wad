<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // membuat tabel users
        Schema::create('users', function (Blueprint $table) {
            // menciptakan kolom dengan nilai increment dengan nama id
            $table->bigIncrements('id');
            // menciptakan kolom dengan nama name
            $table->string('name');
            // menciptakan kolom dengan nama email dengan tipe unik sehingga tiap user berbeda email
            $table->string('email')->unique();
            // menciptakan kolom dengan nama email_verified at
            $table->timestamp('email_verified_at')->nullable();
            // menciptakan kolom dengan nama password
            $table->string('password');
            // menciptakan kolom dengan nama phone_number
            $table->string('phone_number')->nullable();
            // menciptakan kolom dengan nama address
            $table->string('address')->nullable();
            // menciptakan kolom dengan nama birthdate
            $table->string('birthdate')->nullable();
            // menciptakan kolom dengan nama token
            $table->rememberToken();
            // menciptakan kolom dengan nama timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
