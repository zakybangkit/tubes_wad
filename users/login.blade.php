@extends('global')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="container">
                <div class="card" style="background-color: #EFDED8;">
                    <h2 class="text-center">Login</h2>
                        <div class="card-body">
                                <input type="text" class="form-control" placeholder="username"> <br>
                                <input type="password" class="form-control" placeholder="password"> <br>
                        </div>
                        <div class="text-center mb-5">
                            <button class="btn btn-primary btn-lg" style="background-color : #EC9696; color : #707070">Login</button>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection